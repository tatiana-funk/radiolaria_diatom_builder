import toxi.geom.*;
import toxi.geom.mesh2d.*;

import toxi.util.*;
import toxi.util.datatypes.*;

import toxi.processing.*;

//import nervoussystem.obj.*;

import processing.opengl.*;

private PFont calibri12;
private PFont calibri20; 
 
//Colors
color white = color(255,255,255);
color lgreen = color(197,245,178);
color dgreen = color(87,173,68);
color lblue = color(141,215,222);
color orange = color(245,189,84);
color brown = color(214,153,93);
color grey = color(216,196,179); 
color pink = color(255,144,165);
color yellow = color(245,236,153);
color purple = color(201,157,219);
color chosencolor = white;
  
// ranges for x/y positions of points
FloatRange xpos, ypos;

// helper class for rendering
ToxiclibsSupport gfx;

// optional polygon clipper
PolygonClipper2D clip;

// switches
boolean doShowPoints = true;
boolean doShowDelaunay;
boolean doShowHelp=true;
boolean doClip = true;
boolean doSave;

boolean triangle = false;
boolean record = false;
float shape = 0.5;
float center = 10.0;
float multiplier = 1.0;
float multiplier2 = 1.0;
float multiplier3 = 1.0;

float rand = 1.5;
float outscale = 1.0;
float outcircle = 0.75;
float showlines = 1;
float show4 = 1;
float cellwidth = 8;
float bevel = 2.0;
float bellbevel = 1.5;
float bellfunc = 8;
float tribevel = 1.5;
float trifunc = 5;

int amt1 = 40;
int amt2 = 140;
int amt3 = 40;

int type = 1;


float rotate = PI/4;
float rotate2 = 0;
float scale = 1;
float zoomx = 1.0;
float zoomy = 1.0;
float zoomz = 1.0;



ConvexPolygonClipper convexClipper;

float ellipsesize = (2.25*outcircle)*center*sqrt(int(amt1*(multiplier*3)+ amt2*(multiplier2*3)+ amt3*(multiplier3)))+multiplier2*10;

void setup() {
  calibri12 = createFont("Calibri-12.vlw",12,true);  
  calibri20 = createFont("Calibri-20.vlw",20,true);  
  size(800,700,P3D);
  resetCanvas();
 
  // focus x positions around horizontal center (w/ 33% standard deviation)
  xpos=new BiasedFloatRange(0, width, width, 0.5f);
  // focus y positions around bottom (w/ 50% standard deviation)
  ypos=new BiasedFloatRange(0, height, height, 0.5f);
  // setup clipper with centered rectangle
  clip=new SutherlandHodgemanClipper(new Rect(50, 50, width-100, height-100));
  gfx = new ToxiclibsSupport(this);

  
}

void draw() {

  resetCanvas();
    lights();
  
    // empty voronoi mesh container
    Voronoi voronoi = new Voronoi();   
  
  int  totalsize = int(amt1*(multiplier*3)+ amt2*(multiplier2*3)+ amt3*(multiplier3));
  drawRadiolaria(totalsize, int(amt1*(multiplier*3)), int(amt2*(multiplier2*3)), int(amt3*(multiplier3*3)), voronoi);

      if (type == 1) {
         drawVoronoiDisplayFlat(voronoi);   
      }
      else if (type == 2) {
         drawVoronoiDisplay(voronoi);   
      }
      else if (type == 3) {
         drawVoronoiDisplayBell(voronoi);  
      }
      else if (type == 4) {
         drawVoronoiDisplay3(voronoi);   
      }
      else if (type == 5) {
         drawVoronoiDisplay4(voronoi);     
      }
      else if (type == 6) {
         drawVoronoiDisplay5(voronoi);     
      }  
      
    /*if (record) {
        beginRaw("nervoussystem.obj.OBJExport", "Diatom.obj"); 
       if (type == 1) {
         drawVoronoiDisplayFlat(voronoi);   
       } 
       else if (type == 2) {
           drawVoronoiDisplay(voronoi);   
        }
        else if (type == 3) {
           drawVoronoiDisplayBell(voronoi);  
        }
        else if (type == 4) {
           drawVoronoiDisplay3(voronoi);    
        }
        else if (type == 5) {
           drawVoronoiDisplay4(voronoi);     
        }  
        else if (type == 6) {
           drawVoronoiDisplay5(voronoi);     
        }  
              
        endRaw();
        fill(255,0,0);
        strokeWeight(1);
       
        text("Done!",width/2-20,50);
        record = false;

    }*/


     drawUI();   
}

void resetCanvas() {
  background(0);
  smooth();
  strokeWeight(1);
}

void drawVoronoiDisplay(Voronoi voronoi) {
  
       for (Polygon2D poly : voronoi.getRegions()) {
         fill(0,0);
         stroke(chosencolor);
         strokeWeight(cellwidth/2);

              pushMatrix();                             
              translate(width/2,height/2,0);
              rotateX( rotate );  
              rotateZ( rotate2 );
              scale(scale,1,1);
              scale(zoomx,zoomy,zoomz);              
              translate(-width/2,-height/2,0);          
              beginShape();
            
               float rsqr = (ellipsesize/2)*(ellipsesize/2); 
                              
                  if (triangle) {
                    
                      for(int i=0,num=poly.vertices.size(); i<num; i++) {
                            PVector point = new PVector(poly.vertices.get(i).x,poly.vertices.get(i).y,0);   
                            PVector point2 = new PVector(poly.vertices.get((i+1)%num).x,poly.vertices.get((i+1)%num).y,0);     
                            PVector A = new PVector(width/2,height/2-ellipsesize/2-10,0); //Triangle edge 1   
                            PVector B = new PVector(width/2-ellipsesize/2+10,height/2+ellipsesize/3.5,0);  //Triangle edge 2
                            PVector C = new PVector(width/2+ellipsesize/2,height/2+ellipsesize/3.5,0);   //Triangle edge 3                  
                            boolean inside = PointInTriangle(point, A, B, C);
                            boolean inside2 = PointInTriangle(point2, A, B, C);  
                            
                               if (inside && inside2) { 
                                 float x = poly.vertices.get(i).x-width/2;
                                 float y = poly.vertices.get(i).y-height/2; 
                                 float length1 = sqrt(x*x+y*y); 
                                 
                                 float x2 = poly.vertices.get((i+1)%num).x-width/2;
                                 float y2 = poly.vertices.get((i+1)%num).y-height/2; 
                                 float length2 = sqrt(x2*x2+y2*y2);     
                        
                                 float z = sqrt(rsqr-x*x-y*y);
                                 float z2 = sqrt(rsqr-x2*x2-y2*y2);         
              
                                   if (length1 <= ellipsesize/2 && length2 <= ellipsesize/2) { 
                                           vertex(poly.vertices.get(i).x,poly.vertices.get(i).y,z/bevel);
                                           vertex(poly.vertices.get((i+1)%num).x,poly.vertices.get((i+1)%num).y,z2/bevel);                                       
                                   }                                    
                               }
                      }
                  }
                  else if (!triangle) {
                     for(int i=0,num=poly.vertices.size(); i<num; i++) {
                         float x = poly.vertices.get(i).x-width/2;
                         float y = poly.vertices.get(i).y-height/2; 
                         float length1 = sqrt(x*x+y*y); 
                         
                         float x2 = poly.vertices.get((i+1)%num).x-width/2;
                         float y2 = poly.vertices.get((i+1)%num).y-height/2; 
                         float length2 = sqrt(x2*x2+y2*y2);     
                
                         float z = sqrt(rsqr-x*x-y*y);
                         float z2 = sqrt(rsqr-x2*x2-y2*y2);         
      
                           if (length1 <= ellipsesize/2 && length2 <= ellipsesize/2) { 
                                   vertex(poly.vertices.get(i).x,poly.vertices.get(i).y,z/bevel);
                                   vertex(poly.vertices.get((i+1)%num).x,poly.vertices.get((i+1)%num).y,z2/bevel);                                       
                           }
                           
                     }
                   }
                  
                                                          
               endShape();
               popMatrix();               
      } 
    
}

void drawVoronoiDisplayBell(Voronoi voronoi) {
  
       for (Polygon2D poly : voronoi.getRegions()) {
         fill(0,0);
         stroke(chosencolor);
         strokeWeight(cellwidth/2);

              pushMatrix();                             
              translate(width/2,height/2,0);
              rotateX( rotate );  
              rotateZ( rotate2 );              
              scale(scale,1,1); 
              scale(zoomx,zoomy,zoomz);     
              translate(-width/2,-height/2,0);          
              beginShape();
            
               float rsqr = (ellipsesize/2)*(ellipsesize/2); 
                              
                  if (triangle) {
                    
                      for(int i=0,num=poly.vertices.size(); i<num; i++) {
                            PVector point = new PVector(poly.vertices.get(i).x,poly.vertices.get(i).y,0);   
                            PVector point2 = new PVector(poly.vertices.get((i+1)%num).x,poly.vertices.get((i+1)%num).y,0);     
                            PVector A = new PVector(width/2,height/2-ellipsesize/2-10,0); //Triangle edge 1   
                            PVector B = new PVector(width/2-ellipsesize/2+10,height/2+ellipsesize/3.5,0);  //Triangle edge 2
                            PVector C = new PVector(width/2+ellipsesize/2,height/2+ellipsesize/3.5,0);   //Triangle edge 3                  
                            boolean inside = PointInTriangle(point, A, B, C);
                            boolean inside2 = PointInTriangle(point2, A, B, C);  
                            
                               if (inside && inside2) { 
                                   float x = poly.vertices.get(i).x-width/2;
                                   float y = poly.vertices.get(i).y-height/2; 
                                   float length1 = sqrt(x*x+y*y); 
                                   
                                   float x2 = poly.vertices.get((i+1)%num).x-width/2;
                                   float y2 = poly.vertices.get((i+1)%num).y-height/2; 
                                   float length2 = sqrt(x2*x2+y2*y2);     
                          
                                   float rad = ellipsesize / bellfunc;
                          
                                   float z = rad*cos(length1 / rad);
                                   float z2 = rad*cos(length2 / rad);         
                
                                     if (length1 <= ellipsesize/2 && length2 <= ellipsesize/2) { 
                                             vertex(poly.vertices.get(i).x,poly.vertices.get(i).y,z/bellbevel);
                                             vertex(poly.vertices.get((i+1)%num).x,poly.vertices.get((i+1)%num).y,z2/bellbevel);                                       
                                     }
                               }
                      }
                  }
                else if (!triangle) {
                     for(int i=0,num=poly.vertices.size(); i<num; i++) {
                         float x = poly.vertices.get(i).x-width/2;
                         float y = poly.vertices.get(i).y-height/2; 
                         float length1 = sqrt(x*x+y*y); 
                         
                         float x2 = poly.vertices.get((i+1)%num).x-width/2;
                         float y2 = poly.vertices.get((i+1)%num).y-height/2; 
                         float length2 = sqrt(x2*x2+y2*y2);     
                
                         float rad = ellipsesize / bellfunc;
                
                         float z = rad*cos(length1 / rad);
                         float z2 = rad*cos(length2 / rad);         
      
                           if (length1 <= ellipsesize/2 && length2 <= ellipsesize/2) { 
                                   vertex(poly.vertices.get(i).x,poly.vertices.get(i).y,z/bellbevel);
                                   vertex(poly.vertices.get((i+1)%num).x,poly.vertices.get((i+1)%num).y,z2/bellbevel);                                       
                           }
                                                  
                      }   
                }             
               endShape();
               popMatrix();               
      } 
    
}

void drawVoronoiDisplay3(Voronoi voronoi) {
  
       for (Polygon2D poly : voronoi.getRegions()) {
         fill(0,0);
         stroke(chosencolor);
         strokeWeight(cellwidth/2);

              pushMatrix();                             
              translate(width/2,height/2,0);
              rotateX( rotate ); 
              rotateZ( rotate2 );
              scale(scale,1,1); 
              scale(zoomx,zoomy,zoomz);              
              translate(-width/2,-height/2,0);          
              beginShape();
            
               float rsqr = (ellipsesize/2)*(ellipsesize/2); 
                              
                  if (triangle) {
                    
                      for(int i=0,num=poly.vertices.size(); i<num; i++) {
                            PVector point = new PVector(poly.vertices.get(i).x,poly.vertices.get(i).y,0);   
                            PVector point2 = new PVector(poly.vertices.get((i+1)%num).x,poly.vertices.get((i+1)%num).y,0);     
                            PVector A = new PVector(width/2,height/2-ellipsesize/2-10,0); //Triangle edge 1   
                            PVector B = new PVector(width/2-ellipsesize/2+10,height/2+ellipsesize/3.5,0);  //Triangle edge 2
                            PVector C = new PVector(width/2+ellipsesize/2,height/2+ellipsesize/3.5,0);   //Triangle edge 3                  
                            boolean inside = PointInTriangle(point, A, B, C);
                            boolean inside2 = PointInTriangle(point2, A, B, C);  
                            
                               if (inside && inside2) { 
                                     float x = poly.vertices.get(i).x-width/2;
                                     float y = poly.vertices.get(i).y-height/2; 
                                     float length1 = sqrt(x*x+y*y); 
                                     
                                     float x2 = poly.vertices.get((i+1)%num).x-width/2;
                                     float y2 = poly.vertices.get((i+1)%num).y-height/2; 
                                     float length2 = sqrt(x2*x2+y2*y2);     
                            
                                     float rad = ellipsesize / trifunc;
                            
                                     float z = -rad*cubeRoot(length1);
                                     float z2 = -rad*cubeRoot(length2);         
                  
                                       if (length1 <= ellipsesize/2 && length2 <= ellipsesize/2) { 
                                               vertex(poly.vertices.get(i).x,poly.vertices.get(i).y,z/tribevel+200);
                                               vertex(poly.vertices.get((i+1)%num).x,poly.vertices.get((i+1)%num).y,z2/tribevel+200);                                       
                                       }
                                                              
                                  }                                 
                               
                      }
                  }

                  else if (!triangle) {
                   for(int i=0,num=poly.vertices.size(); i<num; i++) {
                       float x = poly.vertices.get(i).x-width/2;
                       float y = poly.vertices.get(i).y-height/2; 
                       float length1 = sqrt(x*x+y*y); 
                       
                       float x2 = poly.vertices.get((i+1)%num).x-width/2;
                       float y2 = poly.vertices.get((i+1)%num).y-height/2; 
                       float length2 = sqrt(x2*x2+y2*y2);     
              
                       float rad = ellipsesize / trifunc;
              
                       float z = -rad*cubeRoot(length1);
                       float z2 = -rad*cubeRoot(length2);         
    
                         if (length1 <= ellipsesize/2 && length2 <= ellipsesize/2) { 
                                 vertex(poly.vertices.get(i).x,poly.vertices.get(i).y,z/tribevel+200);
                                 vertex(poly.vertices.get((i+1)%num).x,poly.vertices.get((i+1)%num).y,z2/tribevel+200);                                       
                         }
                                                
                    } 
                  }               
               endShape();
               popMatrix();               
      } 
    
}

void drawVoronoiDisplay4(Voronoi voronoi) {
  
       for (Polygon2D poly : voronoi.getRegions()) {
         fill(0,0);
         stroke(chosencolor);
         strokeWeight(cellwidth/2);

              pushMatrix();                             
              translate(width/2,height/2,0);
              rotateX( rotate ); 
              rotateZ( rotate2 );
              scale(scale,1,1);  
              scale(zoomx,zoomy,zoomz);              
              translate(-width/2,-height/2,0);          
              beginShape();
            
               float rsqr = (ellipsesize/2)*(ellipsesize/2); 

                  if (triangle) {
                    
                      for(int i=0,num=poly.vertices.size(); i<num; i++) {
                            PVector point = new PVector(poly.vertices.get(i).x,poly.vertices.get(i).y,0);   
                            PVector point2 = new PVector(poly.vertices.get((i+1)%num).x,poly.vertices.get((i+1)%num).y,0);     
                            PVector A = new PVector(width/2,height/2-ellipsesize/2-10,0); //Triangle edge 1   
                            PVector B = new PVector(width/2-ellipsesize/2+10,height/2+ellipsesize/3.5,0);  //Triangle edge 2
                            PVector C = new PVector(width/2+ellipsesize/2,height/2+ellipsesize/3.5,0);   //Triangle edge 3                  
                            boolean inside = PointInTriangle(point, A, B, C);
                            boolean inside2 = PointInTriangle(point2, A, B, C);  
                            
                               if (inside && inside2) { 
                                   float x = poly.vertices.get(i).x-width/2;
                                   float y = poly.vertices.get(i).y-height/2; 
                                   float length1 = sqrt(x*x+y*y); 
                                   
                                   float x2 = poly.vertices.get((i+1)%num).x-width/2;
                                   float y2 = poly.vertices.get((i+1)%num).y-height/2; 
                                   float length2 = sqrt(x2*x2+y2*y2);     
                          
                                   float z = sqrt(rsqr-x*x-y*y);
                                   float z2 = sqrt(rsqr-x2*x2-y2*y2);
                                   
                                   float rad = ellipsesize / bellfunc;                  
                                   float z3 = rad*cos(length1 / rad);
                                   float z4 = rad*cos(length2 / rad); 
                
                                     if (length1 <= ellipsesize/2 && length1 > ellipsesize/4 && length2 <= ellipsesize/2 && length2 > ellipsesize/4) { 
                                             vertex(poly.vertices.get(i).x,poly.vertices.get(i).y,z/bevel);
                                             vertex(poly.vertices.get((i+1)%num).x,poly.vertices.get((i+1)%num).y,z2/bevel);    
                                         
                                     }
                                     if (length1 <= ellipsesize/2 && length1 <= ellipsesize/4 && length2 <= ellipsesize/2 && length2 > ellipsesize/4) { 
                                             vertex(poly.vertices.get(i).x,poly.vertices.get(i).y,z/bevel + z3/bellbevel);
                                             vertex(poly.vertices.get((i+1)%num).x,poly.vertices.get((i+1)%num).y,z2/bevel);                                       
                                     }
                                     if (length1 <= ellipsesize/2 && length1 > ellipsesize/4 && length2 <= ellipsesize/2 && length2 <= ellipsesize/4) { 
                                             vertex(poly.vertices.get(i).x,poly.vertices.get(i).y,z/bevel);
                                             vertex(poly.vertices.get((i+1)%num).x,poly.vertices.get((i+1)%num).y,z2/bevel + z4/bellbevel);                                       
                                     }
                                     if (length1 <= ellipsesize/4 && length2 <= ellipsesize/4) { 
                                             vertex(poly.vertices.get(i).x,poly.vertices.get(i).y,z/bevel + z3/bellbevel);
                                             vertex(poly.vertices.get((i+1)%num).x,poly.vertices.get((i+1)%num).y,z2/bevel + z4/bellbevel);                                       
                                     }                                   
                               }
                      }
                      
                  }


                    else if (!triangle) {
                         for(int i=0,num=poly.vertices.size(); i<num; i++) {
                             float x = poly.vertices.get(i).x-width/2;
                             float y = poly.vertices.get(i).y-height/2; 
                             float length1 = sqrt(x*x+y*y); 
                             
                             float x2 = poly.vertices.get((i+1)%num).x-width/2;
                             float y2 = poly.vertices.get((i+1)%num).y-height/2; 
                             float length2 = sqrt(x2*x2+y2*y2);     
                    
                             float z = sqrt(rsqr-x*x-y*y);
                             float z2 = sqrt(rsqr-x2*x2-y2*y2);
                             
                             float rad = ellipsesize / bellfunc;                  
                             float z3 = rad*cos(length1 / rad);
                             float z4 = rad*cos(length2 / rad); 
          
                               if (length1 <= ellipsesize/2 && length1 > ellipsesize/4 && length2 <= ellipsesize/2 && length2 > ellipsesize/4) { 
                                       vertex(poly.vertices.get(i).x,poly.vertices.get(i).y,z/bevel);
                                       vertex(poly.vertices.get((i+1)%num).x,poly.vertices.get((i+1)%num).y,z2/bevel);    
                                   
                               }
                               if (length1 <= ellipsesize/2 && length1 <= ellipsesize/4 && length2 <= ellipsesize/2 && length2 > ellipsesize/4) { 
                                       vertex(poly.vertices.get(i).x,poly.vertices.get(i).y,z/bevel + z3/bellbevel);
                                       vertex(poly.vertices.get((i+1)%num).x,poly.vertices.get((i+1)%num).y,z2/bevel);                                       
                               }
                               if (length1 <= ellipsesize/2 && length1 > ellipsesize/4 && length2 <= ellipsesize/2 && length2 <= ellipsesize/4) { 
                                       vertex(poly.vertices.get(i).x,poly.vertices.get(i).y,z/bevel);
                                       vertex(poly.vertices.get((i+1)%num).x,poly.vertices.get((i+1)%num).y,z2/bevel + z4/bellbevel);                                       
                               }
                               if (length1 <= ellipsesize/4 && length2 <= ellipsesize/4) { 
                                       vertex(poly.vertices.get(i).x,poly.vertices.get(i).y,z/bevel + z3/bellbevel);
                                       vertex(poly.vertices.get((i+1)%num).x,poly.vertices.get((i+1)%num).y,z2/bevel + z4/bellbevel);                                       
                               }  
                                                 
                                                      
                          }
                    }                                                                              
               endShape();
               popMatrix();               
      } 
    
}

void drawVoronoiDisplay5(Voronoi voronoi) {
  
       for (Polygon2D poly : voronoi.getRegions()) {
         fill(0,0);
         stroke(chosencolor);
         strokeWeight(cellwidth/2);

              pushMatrix();                             
              translate(width/2,height/2,0);
              rotateX( rotate ); 
              rotateZ( rotate2 );
              scale(scale,1,1);
              scale(zoomx,zoomy,zoomz);           
              translate(-width/2,-height/2,0);          
              beginShape();
            
               float rsqr = (ellipsesize/2)*(ellipsesize/2); 

                  if (triangle) {
                    
                      for(int i=0,num=poly.vertices.size(); i<num; i++) {
                            PVector point = new PVector(poly.vertices.get(i).x,poly.vertices.get(i).y,0);   
                            PVector point2 = new PVector(poly.vertices.get((i+1)%num).x,poly.vertices.get((i+1)%num).y,0);     
                            PVector A = new PVector(width/2,height/2-ellipsesize/2-10,0); //Triangle edge 1   
                            PVector B = new PVector(width/2-ellipsesize/2+10,height/2+ellipsesize/3.5,0);  //Triangle edge 2
                            PVector C = new PVector(width/2+ellipsesize/2,height/2+ellipsesize/3.5,0);   //Triangle edge 3                  
                            boolean inside = PointInTriangle(point, A, B, C);
                            boolean inside2 = PointInTriangle(point2, A, B, C);  
                            
                               if (inside && inside2) { 
                                   float x = poly.vertices.get(i).x-width/2;
                                   float y = poly.vertices.get(i).y-height/2; 
                                   float length1 = sqrt(x*x+y*y); 
                                   
                                   float x2 = poly.vertices.get((i+1)%num).x-width/2;
                                   float y2 = poly.vertices.get((i+1)%num).y-height/2; 
                                   float length2 = sqrt(x2*x2+y2*y2);     
                          
                                   float z = sqrt(rsqr-x*x-y*y);
                                   float z2 = sqrt(rsqr-x2*x2-y2*y2);
                                   
                                   float rad = ellipsesize / bellfunc;                  
                                   float z3 = rad*cos(length1 / rad);
                                   float z4 = rad*cos(length2 / rad); 
                
                                   float rad2 = ellipsesize;                   
                                   float z5 = -rad2*cubeRoot(length1);  
                                   float z6 = -rad2*cubeRoot(length2);         
                
                                     if (length1 <= ellipsesize/8 && length2 <= ellipsesize/8) { 
                                             vertex(poly.vertices.get(i).x,poly.vertices.get(i).y,z/bevel + z3/bellbevel+ z5/10+ellipsesize/3);
                                             vertex(poly.vertices.get((i+1)%num).x,poly.vertices.get((i+1)%num).y,z2/bevel + z4/bellbevel + z6/10+ellipsesize/3);                                       
                                     }
                                     else if (length1 <= ellipsesize/8 && length2 >= ellipsesize/8) { 
                                             vertex(poly.vertices.get(i).x,poly.vertices.get(i).y,z/bevel + z3/bellbevel+ z5/10+ellipsesize/3);
                                             vertex(poly.vertices.get((i+1)%num).x,poly.vertices.get((i+1)%num).y,z2/bevel + z4/bellbevel); 
                                     }
                
                                     else if (length1 >= ellipsesize/8 && length2 <= ellipsesize/8) { 
                                             vertex(poly.vertices.get(i).x,poly.vertices.get(i).y,z/bevel + z3/bellbevel);
                                             vertex(poly.vertices.get((i+1)%num).x,poly.vertices.get((i+1)%num).y,z2/bevel + z4/bellbevel + z6/10+ellipsesize/3);  
                                     }
                
                                     else if (length1 <= ellipsesize/2 && length1 > ellipsesize/4 && length2 <= ellipsesize/2 && length2 > ellipsesize/4) { 
                                             vertex(poly.vertices.get(i).x,poly.vertices.get(i).y,z/bevel);
                                             vertex(poly.vertices.get((i+1)%num).x,poly.vertices.get((i+1)%num).y,z2/bevel);                             
                                     }
                                    else  if (length1 <= ellipsesize/2 && length1 <= ellipsesize/4 && length2 <= ellipsesize/2 && length2 > ellipsesize/4) { 
                                             vertex(poly.vertices.get(i).x,poly.vertices.get(i).y,z/bevel + z3/bellbevel);
                                             vertex(poly.vertices.get((i+1)%num).x,poly.vertices.get((i+1)%num).y,z2/bevel);                                       
                                     }
                                     else if (length1 <= ellipsesize/2 && length1 > ellipsesize/4 && length2 <= ellipsesize/2 && length2 <= ellipsesize/4) { 
                                             vertex(poly.vertices.get(i).x,poly.vertices.get(i).y,z/bevel);
                                             vertex(poly.vertices.get((i+1)%num).x,poly.vertices.get((i+1)%num).y,z2/bevel + z4/bellbevel);                                       
                                     }
                                     else if (length1 <= ellipsesize/4 && length2 <= ellipsesize/4) { 
                                             vertex(poly.vertices.get(i).x,poly.vertices.get(i).y,z/bevel + z3/bellbevel);
                                             vertex(poly.vertices.get((i+1)%num).x,poly.vertices.get((i+1)%num).y,z2/bevel + z4/bellbevel);                                       
                                     }                                  
                               }
                      }
                  }
                  
                  
                  else if (!triangle) {
               for(int i=0,num=poly.vertices.size(); i<num; i++) {
                       float x = poly.vertices.get(i).x-width/2;
                       float y = poly.vertices.get(i).y-height/2; 
                       float length1 = sqrt(x*x+y*y); 
                       
                       float x2 = poly.vertices.get((i+1)%num).x-width/2;
                       float y2 = poly.vertices.get((i+1)%num).y-height/2; 
                       float length2 = sqrt(x2*x2+y2*y2);     
              
                       float z = sqrt(rsqr-x*x-y*y);
                       float z2 = sqrt(rsqr-x2*x2-y2*y2);
                       
                       float rad = ellipsesize / bellfunc;                  
                       float z3 = rad*cos(length1 / rad);
                       float z4 = rad*cos(length2 / rad); 
    
                       float rad2 = ellipsesize;                   
                       float z5 = -rad2*cubeRoot(length1);  
                       float z6 = -rad2*cubeRoot(length2);         
    
                         if (length1 <= ellipsesize/8 && length2 <= ellipsesize/8) { 
                                 vertex(poly.vertices.get(i).x,poly.vertices.get(i).y,z/bevel + z3/bellbevel+ z5/10+ellipsesize/3);
                                 vertex(poly.vertices.get((i+1)%num).x,poly.vertices.get((i+1)%num).y,z2/bevel + z4/bellbevel + z6/10+ellipsesize/3);                                       
                         }
                         else if (length1 <= ellipsesize/8 && length2 >= ellipsesize/8) { 
                                 vertex(poly.vertices.get(i).x,poly.vertices.get(i).y,z/bevel + z3/bellbevel+ z5/10+ellipsesize/3);
                                 vertex(poly.vertices.get((i+1)%num).x,poly.vertices.get((i+1)%num).y,z2/bevel + z4/bellbevel); 
                         }
    
                         else if (length1 >= ellipsesize/8 && length2 <= ellipsesize/8) { 
                                 vertex(poly.vertices.get(i).x,poly.vertices.get(i).y,z/bevel + z3/bellbevel);
                                 vertex(poly.vertices.get((i+1)%num).x,poly.vertices.get((i+1)%num).y,z2/bevel + z4/bellbevel + z6/10+ellipsesize/3);  
                         }
    
                         else if (length1 <= ellipsesize/2 && length1 > ellipsesize/4 && length2 <= ellipsesize/2 && length2 > ellipsesize/4) { 
                                 vertex(poly.vertices.get(i).x,poly.vertices.get(i).y,z/bevel);
                                 vertex(poly.vertices.get((i+1)%num).x,poly.vertices.get((i+1)%num).y,z2/bevel);                             
                         }
                        else  if (length1 <= ellipsesize/2 && length1 <= ellipsesize/4 && length2 <= ellipsesize/2 && length2 > ellipsesize/4) { 
                                 vertex(poly.vertices.get(i).x,poly.vertices.get(i).y,z/bevel + z3/bellbevel);
                                 vertex(poly.vertices.get((i+1)%num).x,poly.vertices.get((i+1)%num).y,z2/bevel);                                       
                         }
                         else if (length1 <= ellipsesize/2 && length1 > ellipsesize/4 && length2 <= ellipsesize/2 && length2 <= ellipsesize/4) { 
                                 vertex(poly.vertices.get(i).x,poly.vertices.get(i).y,z/bevel);
                                 vertex(poly.vertices.get((i+1)%num).x,poly.vertices.get((i+1)%num).y,z2/bevel + z4/bellbevel);                                       
                         }
                         else if (length1 <= ellipsesize/4 && length2 <= ellipsesize/4) { 
                                 vertex(poly.vertices.get(i).x,poly.vertices.get(i).y,z/bevel + z3/bellbevel);
                                 vertex(poly.vertices.get((i+1)%num).x,poly.vertices.get((i+1)%num).y,z2/bevel + z4/bellbevel);                                       
                         }  
                                           
                                                
                    }
                  }                                                                                     
               endShape();
               popMatrix();               
      } 
    
}

void drawVoronoiDisplayFlat(Voronoi voronoi) {
  
       for (Polygon2D poly : voronoi.getRegions()) {
         fill(0,0);
         stroke(chosencolor);
         strokeWeight(cellwidth/2);

              pushMatrix();                             
              translate(width/2,height/2,0);
              rotateX( rotate );  
              rotateZ( rotate2 );
              scale(scale,1,1);
              scale(zoomx,zoomy,zoomz);              
              translate(-width/2,-height/2,0);          
              beginShape();
            
                  if (triangle) {
                    
                      for(int i=0,num=poly.vertices.size(); i<num; i++) {
                            PVector point = new PVector(poly.vertices.get(i).x,poly.vertices.get(i).y,0);   
                            PVector point2 = new PVector(poly.vertices.get((i+1)%num).x,poly.vertices.get((i+1)%num).y,0);     
                            PVector A = new PVector(width/2,height/2-ellipsesize/2-10,0); //Triangle edge 1   
                            PVector B = new PVector(width/2-ellipsesize/2+10,height/2+ellipsesize/3.5,0);  //Triangle edge 2
                            PVector C = new PVector(width/2+ellipsesize/2,height/2+ellipsesize/3.5,0);   //Triangle edge 3                  
                            boolean inside = PointInTriangle(point, A, B, C);
                            boolean inside2 = PointInTriangle(point2, A, B, C);  
                            
                               if (inside && inside2) { 
                                     float x = poly.vertices.get(i).x-width/2;
                                     float y = poly.vertices.get(i).y-height/2; 
                                     float length1 = sqrt(x*x+y*y); 
                                     
                                     float x2 = poly.vertices.get((i+1)%num).x-width/2;
                                     float y2 = poly.vertices.get((i+1)%num).y-height/2; 
                                     float length2 = sqrt(x2*x2+y2*y2);     
                                 
                  
                                       if (length1 <= ellipsesize/2 && length2 <= ellipsesize/2) { 
                                               vertex(poly.vertices.get(i).x,poly.vertices.get(i).y,0);
                                               vertex(poly.vertices.get((i+1)%num).x,poly.vertices.get((i+1)%num).y,0);                                       
                                       }                                 
                               }
                               
                      }
                  }

                   else if (!triangle) {       
                         for(int i=0,num=poly.vertices.size(); i<num; i++) {
                             float x = poly.vertices.get(i).x-width/2;
                             float y = poly.vertices.get(i).y-height/2; 
                             float length1 = sqrt(x*x+y*y); 
                             
                             float x2 = poly.vertices.get((i+1)%num).x-width/2;
                             float y2 = poly.vertices.get((i+1)%num).y-height/2; 
                             float length2 = sqrt(x2*x2+y2*y2);     
                         
          
                               if (length1 <= ellipsesize/2 && length2 <= ellipsesize/2) { 
                                       vertex(poly.vertices.get(i).x,poly.vertices.get(i).y,0);
                                       vertex(poly.vertices.get((i+1)%num).x,poly.vertices.get((i+1)%num).y,0);                                       
                               }
                                                      
                          }
                   }
               endShape();
               popMatrix();               
      } 
    
}



boolean SameSide(PVector p1,PVector p2, PVector a, PVector b) {
    PVector first = new PVector(b.x-a.x,b.y-a.y,0);
    PVector second = new PVector(p1.x-a.x,p1.y-a.y,0);
    PVector second2 = new PVector(p2.x-a.x,p2.y-a.y,0);

    PVector cp1 = first.cross(second);   
    PVector cp2 = first.cross(second2); 
      if (cp1.dot(cp2) >= 0) { 
        return true;
      }
      else {
        return false;
      }
} 

boolean PointInTriangle(PVector p, PVector a, PVector b, PVector c) {
    if (SameSide(p,a,b,c) && SameSide(p,b, a,c) && SameSide(p,c, a,b)) {
       return true;
    }
    else {
      return false;
    }
}

void drawRadiolaria(int totalsize, int num, int num2, int num3, Voronoi voronoi){
  
  float ellipse_size = (2.25*outcircle)*center*sqrt(totalsize)+multiplier2*10;
  ellipsesize = ellipse_size;

    float originX = width/2.0;
    float originY = height/2.0;
    
   float c = center;//r*multiplier;
   float incr = (2*PI)/180; 


    if (triangle) {
        //A-B
/*
                  PVector A = new PVector(width/2,height/2-ellipsesize/2,0); //Triangle edge 1   
                  PVector B = new PVector(width/2-ellipsesize/2,height/2+ellipsesize/3.3,0);  //Triangle edge 2
                  PVector C = new PVector(width/2+ellipsesize/2,height/2+ellipsesize/3.3,0);   //Triangle edge 3
 */                 
        int portion = 60;
        float ABx = abs((10+width/2-(width/2-ellipsesize/2))/portion);
        float ABy = abs(((10+height/2-ellipsesize/2)-(height/2+ellipsesize/3.5))/portion);
        for (int i = 0; i < portion; i++) {         
          float xval = width/2 + ABx*i;
          float yval = height/2-ellipsesize/2 + ABy*i;
          voronoi.addPoint(new Vec2D(xval,yval));       
        }      
        
        float BCx = ellipsesize/portion;
        for (int i = 0; i < portion; i++) {         
          float xval = width/2-ellipsesize/2 + BCx*i;
          float yval = height/2+ellipsesize/3.5;
          voronoi.addPoint(new Vec2D(xval,yval));       
        } 
    
        float ACx = ((width/2+ellipsesize/2)-(width/2))/portion;
        float ACy = ((height/2+ellipsesize/3.5)-(height/2-ellipsesize/2))/portion;

        for (int i = 0; i < portion; i++) {         
          float xval = (width/2-ellipsesize/2) + ACx*i;
          float yval = (height/2+ellipsesize/3.5) - ACy*i;
          voronoi.addPoint(new Vec2D(xval,yval));       
        }  

        
    }
    else { //if sphere
        for (int i = 0; i < 180; i++) {
          
          float xval = (ellipse_size/2)*cos(incr*i)+width/2;
          float yval = (ellipse_size/2)*sin(incr*i)+height/2;      
          voronoi.addPoint(new Vec2D(xval,yval));       
        }
    }

    for(int i=0; i < num+num2+num3; i++) {

        if (i <= num) {      
          float radius = c * sqrt(i)+(multiplier*rand*2);
          float currentAngle = i * (137.5)+amt1;
  
          float y = -1 * radius * sin(currentAngle);
          float x = radius * cos(currentAngle);
  
          float drawX = originX + x;
          float drawY = originY + y;
  
          voronoi.addPoint(new Vec2D(drawX, drawY));      

        }
        
        else if (i > num && i <= num+num2) {

          float radius = c/1.25 * sqrt(i);
          float currentAngle = i * (137.5)+amt2;
  
          float y = -1 * radius * sin(currentAngle);
          float x = radius * cos(currentAngle);
  
          float drawX = originX + x;
          float drawY = originY + y;
   
          voronoi.addPoint(new Vec2D(drawX, drawY));      
        }
        
        else if (i > num+num2 && i <= num+num2+num3) {
         
          float radius = (c * sqrt(i))*outscale;
          float currentAngle = i *(30)+amt3;
  
          float y = -1 * radius * sin(currentAngle)/outscale;
          float x = radius * cos(currentAngle)/outscale;
  
          float drawX = originX + x;
          float drawY = originY + y;
          
          voronoi.addPoint(new Vec2D(drawX, drawY));      
                  
        }
        
    }
     int ts = int(totalsize/2.1); 
     int ts2 = int(totalsize/2.5); 
     if (showlines > 1.75) {   
         for (int i = 0; i < ts; i++) { 
              voronoi.addPoint(new Vec2D(width/2-(ellipsesize/2)+((ellipsesize/ts)*i), height/2));  //left-right    
              voronoi.addPoint(new Vec2D(width/2,height/2-(ellipsesize/2)+((ellipsesize/ts)*i)));   //up-down
              
              if (show4 >= 1.25) {
              voronoi.addPoint(new Vec2D(width/2-(ellipsesize/2)+((ellipsesize/ts2)*i), height/2-(ellipsesize/2)+((ellipsesize/ts2)*i)));  //up-left,bottom-right    
              voronoi.addPoint(new Vec2D(width/2-(ellipsesize/2)+((ellipsesize/ts2)*i), height/2+(ellipsesize/2)-((ellipsesize/ts2)*i)));  //bottom-left,up-right                                     
              }
     
         }
     }
 
}


float cubeRoot(float d) {
  if (d < 0.0) {
    return -cubeRoot(-d);
  }
  else {
    return pow(d,1.0/3.0);
  }
}

void drawUI() {
  strokeWeight(0);
  fill(175);   
  textMode(SCREEN);   
  rect(10,10,15,15); //Change shape to 1
  rect(30,10,15,15); //Change shape to 2
  rect(50,10,15,15); //Change shape to 3
  rect(70,10,15,15); //Change shape to 4
  rect(90,10,15,15); //Change shape to 5
  rect(110,10,15,15); //Change shape to 6

  stroke(175);
  strokeWeight(2); 
  fill(0,0); 
  ellipse(195,17,15,15); //Change shape to 6  
  beginShape();
  line(207,25,215,10); 
  line(215,10,223,25);
  line(223,25,207,25);
  endShape();
  strokeWeight(0);
  stroke(0,0);
  
  fill(175);    
  rect(10,35,15,15); //Inner size -
  rect(30,35,15,15); //Inner size +
  
  rect(10,60,15,15); //Outer size -
  rect(30,60,15,15); //Outer size +
  
  rect(10,85,15,15); //Ridge Design 1
  rect(30,85,15,15); //Ridge Design 2
  rect(50,85,15,15); //Ridge Design 3
  
  rect(10,110,15,15); //wall thickness -
  rect(30,110,15,15); // wall thickness +
  
  rect(10,140,50,15); //randomize
 // rect(10,160,50,15); // save to obj

  textFont(calibri20, 20);  
  fill(225);
  text("Radiolaria and", width-130, 22); 
  text("Diatom Builder", width-130, 40); 
  fill(175); 
  textFont(calibri12, 12);
  text("Tatiana Funk", width-100, 60);   
  text("Shape -->", 130, 22);
  text("Change Inner Size", 50, 48);
  text("Change Outer Size", 50, 73);
  text("Ridge Design", 75, 98);
  text("Wall Thickness", 50, 123);
  text("Randomize", 65, 152);
 // text("Save to .obj", 65, 172);
  
  text("W/S - Rotate up/down", 12, 195);
  text("A/D - Shrink/elongate", 12, 212);
  text("Z/X - Zoom out/in", 12, 229);
  text("R/F - Reverse/forward Spin", 12, 246);
  text("Change Color:", 12, 264);  

  textFont(calibri12, 14); 
  fill(255);
  rect(12,12,11,11); //Change shape to 1
  rect(32,12,11,11); //Change shape to 2
  rect(52,12,11,11); //Change shape to 3
  rect(72,12,11,11); //Change shape to 4
  rect(92,12,11,11); //Change shape to 5
  rect(112,12,11,11); //Change shape to 5
  
  rect(12,37,11,11); //Inner size -
  rect(32,37,11,11); //Inner size +
  
  rect(12,62,11,11); //Outer size -
  rect(32,62,11,11); //Outer size +
  
  rect(12,87,11,11); //Ridge Design 1
  rect(32,87,11,11); //Ridge Design 2
  rect(52,87,11,11); //Ridge Design 3
  
  rect(12,112,11,11); //wall thickness -
  rect(32,112,11,11); // wall thickness +
  
  rect(13,143,44,9); //randomize
  //rect(13,163,44,9); // save to obj
  fill(0);
  text("1", 15, 22);
  text("2", 34, 22);  
  text("3", 53, 22); 
  text("4", 74, 22); 
  text("5", 94, 22); 
  text("6", 114, 22); 
  text("-", 16, 46);
  text("+", 33, 47);  
  text("-", 16, 71);
  text("+", 33, 72); 
  text("1", 15, 97);
  text("2", 34, 97);  
  text("3", 53, 97); 
  text("-", 16, 122);
  text("+", 33, 122); 
    
  fill(white);
  rect(13,270,10,10);
  fill(grey);
  rect(25,270,10,10);
  fill(dgreen);
  rect(37,270,10,10);
  fill(lgreen);
  rect(49,270,10,10);
  fill(lblue);
  rect(61,270,10,10);
  fill(brown);
  rect(13,283,10,10);
  fill(orange);
  rect(25,283,10,10);
  fill(yellow);
  rect(37,283,10,10);
  fill(pink);
  rect(49,283,10,10);
  fill(purple);
  rect(61,283,10,10);
}


void mousePressed() {

   //Change shape to 1
   if (mouseX >= 10 && mouseX <= 25 && mouseY >= 10 && mouseY <= 25) {
        type = 1;
        zoomx = 1.0;
        zoomy = 1.0;
        zoomz = 1.0;
   }
  //Change shape to 2
   if (mouseX >= 30 && mouseX <= 45 && mouseY >= 10 && mouseY <= 25) {
        type = 2;
        bevel = random(1,3);
        zoomx = 1.0;
        zoomy = 1.0;
        zoomz = 1.0;
   }
  //Change shape to 3
   if (mouseX >= 50 && mouseX <= 65 && mouseY >= 10 && mouseY <= 25) {
        type = 3;
        bellbevel = random(0.5,2.5);
        bellfunc = random(3,12);
          if(bellfunc > 9) {
             bellbevel = 2.0; 
          }
        zoomx = 1.0;
        zoomy = 1.0;
        zoomz = 1.0;
   }
  //Change shape to 4
   if (mouseX >= 70 && mouseX <= 85 && mouseY >= 10 && mouseY <= 25) {
        type = 4;
        trifunc = random(4.5,18);
        tribevel = random(1,2);
        zoomx = 1.0;
        zoomy = 1.0;
        zoomz = 1.0;
   }

  //Change shape to 5
   if (mouseX >= 90 && mouseX <= 105 && mouseY >= 10 && mouseY <= 25) {
        type = 5;
        bevel = random(1,3);
        bellbevel = random(0.3,2.4);
        bellfunc = random(5,8);
        zoomx = 1.0;
        zoomy = 1.0;
        zoomz = 1.0;
   }
   //Change shape to 6
   if (mouseX >= 110 && mouseX <= 125 && mouseY >= 10 && mouseY <= 25) {
        type = 6;
        bevel = random(1,3);
        bellbevel = random(0.3,2.4);
        bellfunc = random(5,8);
        trifunc = random(4.5,18);
        tribevel = random(1,2);
        zoomx = 1.0;
        zoomy = 1.0;
        zoomz = 1.0;
   }


  if (mouseX >= 188 && mouseX <= 203 && mouseY >= 10 && mouseY <= 25) {
      triangle = false;
  }
  if (mouseX >= 207 && mouseX <= 223 && mouseY >= 10 && mouseY <= 25) {
      triangle = true;
  }
  
  //Inner size -
  if (mouseX >= 10 && mouseX <= 25 && mouseY >= 35 && mouseY <= 50) {
      if (multiplier >= 0.2) {
          multiplier = multiplier - 0.2;
      }
  }
  //Inner size +
  if (mouseX >= 30 && mouseX <= 45 && mouseY >= 35 && mouseY <= 50) {
      if (multiplier <= 2.0) {
          multiplier = multiplier + 0.2;
      }
  }   
  //Outer size -
  if (mouseX >= 10 && mouseX <= 25 && mouseY >= 60 && mouseY <= 75) {
      if (multiplier2 >= 0.2) {
          multiplier2 = multiplier2 - 0.2;
      }
  }
  //Outer size +
  if (mouseX >= 30 && mouseX <= 45 && mouseY >= 60 && mouseY <= 75) {
      if (multiplier2 <= 2.0) {
          multiplier2 = multiplier2 + 0.2;
      }
  } 

  //Ridge Design 1
  if (mouseX >= 10 && mouseX <= 25 && mouseY >= 85 && mouseY <= 100) {
       fill(0,50);
      rect(10,85,15,15); //Ridge Design 1
      showlines = 1.0; //false
  } 
  
  //Ridge Design 2
  if (mouseX >= 30 && mouseX <= 45 && mouseY >= 85 && mouseY <= 100) {
      showlines = 2.0; //true
      show4 = 0.5; //false
  }   

  //Ridge Design 3
  if (mouseX >= 50 && mouseX <= 65 && mouseY >= 85 && mouseY <= 100) {
      showlines = 2.0; //true
      show4 = 1.5; //false
  }

  //Wall Thickness -
  if (mouseX >= 10 && mouseX <= 25 && mouseY >= 110 && mouseY <= 125) {
        if (cellwidth >= 3) {
          cellwidth = cellwidth - 1;
        }
  }  
  //Wall Thickness +
  if (mouseX >= 30 && mouseX <= 45 && mouseY >= 110 && mouseY <= 125) {
        if (cellwidth <= 15) {
          cellwidth = cellwidth + 1;
        }
  }
  //Randomize
  if (mouseX >= 10 && mouseX <= 60 && mouseY >= 140 && mouseY <= 155) {
        center = random(5,10);
        type = int((random(6,36)/6));
        multiplier = random(0.25,1.0);
        multiplier2 = random(0.25,1.75);
        multiplier3 = random(1,2);
        rand = random(0.5,2);
        outscale = random(1.0,1.3);
        shape = random(0,2);
        if (shape <= 1.3) {
          triangle = false;
        }
        else {
           triangle = true; 
        }
        
        showlines = random(1,3);
        show4 = random(0,2);
        amt1 = int(random(40,100));
        amt2 = int(random(40,100));
        amt3 = int(random(60,100));
        cellwidth = random(5,8);
        bevel = random(1,3);
        bellbevel = random(1.0,2.5);
        bellfunc = random(3,12);
          if(bellfunc > 9) {
             bellbevel = 2.0; 
          }
        trifunc = random(4.5,12);
        tribevel = random(1,2);
        
          if (multiplier3 > 1.25) {
             outcircle = 0.65;  
          }
          else {
             outcircle = 0.85; 
          }
        rotate = PI/4;
        scale = random(0.8,1.2);
        findChosenColor(random(0,10.5));
        zoomx = 1.0;
        zoomy = 1.0;
        zoomz = 1.0;        
  }

  //Record
  if (mouseX >= 10 && mouseX <= 60 && mouseY >= 160 && mouseY <= 175) {
       record = true;
  } 
  
  if (mouseX >= 13 && mouseX <= 23 && mouseY >= 270 && mouseY <= 280) {
       chosencolor = white;
  }  
  if (mouseX >= 25 && mouseX <= 35 && mouseY >= 270 && mouseY <= 280) {
       chosencolor = grey;
  } 
  if (mouseX >= 37 && mouseX <= 47 && mouseY >= 270 && mouseY <= 280) {
       chosencolor = dgreen;
  } 
  if (mouseX >= 49 && mouseX <= 59 && mouseY >= 270 && mouseY <= 280) {
       chosencolor = lgreen;
  } 
  if (mouseX >= 61 && mouseX <= 71 && mouseY >= 270 && mouseY <= 280) {
       chosencolor = lblue;
  } 

  if (mouseX >= 13 && mouseX <= 23 && mouseY >= 283 && mouseY <= 293) {
       chosencolor = brown;
  }  
  if (mouseX >= 25 && mouseX <= 35 && mouseY >= 283 && mouseY <= 293) {
       chosencolor = orange;
  } 
  if (mouseX >= 37 && mouseX <= 47 && mouseY >= 283 && mouseY <= 293) {
       chosencolor = yellow;
  } 
  if (mouseX >= 49 && mouseX <= 59 && mouseY >= 283 && mouseY <= 293) {
       chosencolor = pink;
  } 
  if (mouseX >= 61 && mouseX <= 71 && mouseY >= 283 && mouseY <= 293) {
       chosencolor = purple;
  } 

}

void keyPressed() {
  switch (key) {
    case 'w':
      rotate = rotate + PI/12;
      break;
    case 's':
      rotate = rotate - PI/12;
      break;
    case 'a':
      if (scale > 0.3) {
        scale = scale-0.05;
      }
      break;
    case 'd':
      if (scale < 2.0) {
        scale = scale+0.05;
      }
      break;

    case 'z':
      if (zoomx > 0.2) {
        zoomx = zoomx - 0.1;
        zoomy = zoomy - 0.1;
        zoomz = zoomz - 0.1;        
      }
      break;
      
    case 'x':
      if (zoomx < 2.0) {
        zoomx = zoomx + 0.1;
        zoomy = zoomy + 0.1;
        zoomz = zoomz + 0.1;        
      }
      break;
      
    case 'r':
       rotate2 = rotate2+PI/8;
      break;
    case 'f':
       rotate2 = rotate2-PI/8;
      break;    
  }
  
}

void findChosenColor(float rand) {

    if (rand <= 1) {
      chosencolor = lgreen;
    }
    else if (rand > 1 && rand <= 2) {
      chosencolor = dgreen;
    }  
    else if (rand > 2 && rand <= 3) {
      chosencolor = lblue;
    } 
    else if (rand > 3 && rand <= 4) {
      chosencolor = orange;
    }     
    else if (rand > 4 && rand <= 5) {
      chosencolor = brown;
    }  
    else if (rand > 5 && rand <= 6) {
      chosencolor = grey;
    }   
    else if (rand > 6 && rand <= 7) {
      chosencolor = pink;
    } 
    else if (rand > 7 && rand <= 8) {
      chosencolor = yellow;
    }
    else if (rand > 8 && rand <= 9) {
      chosencolor = purple;
    } 
    else  {
      chosencolor = white;
    }     
}

